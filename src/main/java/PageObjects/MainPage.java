package PageObjects;

import Utils.ElementProperty;
import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@Getter
public class MainPage extends BasePage {

    @FindBy(css = ".banner .img-responsive")
    private WebElement banner;

    @FindBy(css = "#contact-link")
    private WebElement contactLink;

    @FindBy(css = ".login")
    private WebElement login;

    @FindBy(css = ".shop-phone")
    private WebElement shopPhone;

    @FindBy(css = ".search_query")
    private WebElement searchInput;

    @FindBy(css = "button[type='submit']")
    private WebElement buttonSubmit;

    @FindBy(css = ".shopping_cart")
    private WebElement shoppingCart;

    public void checkingBanner(){
        getRequiredElement(banner, ElementProperty.VISIBLE);
        logger.info("Banner is checked");
    }

}


