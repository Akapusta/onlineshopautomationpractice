package PageObjects;

import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

@Getter
public class BestsellersSectionMainPage extends BasePage{

    @FindBy(css = "#homefeatured .ajax_block_product .ajax_add_to_cart_button")
    private List<WebElement> buttonsAddToCart;

    @FindBy(css = ".tab-content .product-image-container")
    private List<WebElement> productsImages;






}
