package PageObjects;

import DriverManager.DriverManager;
import Utils.ElementProperty;
import Utils.URLs;
import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@Getter
public class LoginPage extends BasePage {

    @FindBy(css = ".page-heading")
    private WebElement pageHeading;

    public void navigateToLoginPage(){
        DriverManager.getWebDriver().navigate().to(URLs.loginPageUrl);
        new WebDriverWait(driver, 5, 1000).until(ExpectedConditions.urlToBe(URLs.loginPageUrl));
    }

    public void checkingPageHeading(){
        getRequiredElement(pageHeading, ElementProperty.VISIBLE);
        logger.info("Page header is checked");
    }
}
