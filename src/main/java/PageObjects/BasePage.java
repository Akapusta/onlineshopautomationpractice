package PageObjects;

import Utils.ElementProperty;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import DriverManager.DriverManager;

@Slf4j
public class BasePage {

    protected static Logger logger;

    protected WebDriver driver = DriverManager.getWebDriver();

    public BasePage() {
        PageFactory.initElements(driver, this);
        logger = LogManager.getLogger(this.getClass().getName());
    }

    public void getRequiredElement(WebElement el, ElementProperty requirement){
        try{
            switch (requirement) {
                case VISIBLE -> Assert.assertTrue(el.isDisplayed());
                case ENABLED -> Assert.assertTrue(el.isEnabled());
                case SELECTED -> Assert.assertTrue(el.isSelected());
            }
        } catch (Exception e) {
            log.error("The element is not " + requirement.toString().toLowerCase());
            log.error(e.getMessage());
        }
        logger.info("Required element " + el.getTagName() +" is " + requirement.toString().toLowerCase());
    }
}
