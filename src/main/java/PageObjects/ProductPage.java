package PageObjects;

import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@Getter
public class ProductPage extends BasePage{

    @FindBy(css = "#add_to_cart button")
    private WebElement submitButton;

}
