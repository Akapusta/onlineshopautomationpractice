package PageObjects;

import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@Getter
public class LayerCartPage extends BasePage {

    @FindBy(css = ".layer_cart_product h2")
    private WebElement header;

    @FindBy(css = ".layer_cart_product .cross")
    private WebElement crossIcon;
}
