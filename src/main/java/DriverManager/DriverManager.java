package DriverManager;

import Configuration.Configuration;
import DriverManager.listeners.ListenerForEventOfWebdriver;
import Utils.BrowserFactory;
import Utils.BrowserType;
import lombok.extern.slf4j.Slf4j;
import org.aeonbits.owner.ConfigFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

@Slf4j
public class DriverManager {

    public static Logger logger = LogManager.getLogger(DriverManager.class);

    private static Configuration configuration = ConfigFactory.create(Configuration.class);

    private static WebDriver driver;

    private DriverManager() {
    }

    public static WebDriver getWebDriver() {
        if (driver == null) {
            driver = BrowserFactory.getBrowser(BrowserType.valueOf(configuration.localBrowser().toUpperCase()));
        }
        return driver;
    }

    public static void setWebDriver() {
        driver = ListenerForEventOfWebdriver.registerWebDriverEventListener(driver);
        logger.info("Set webdriver");
    }

    public static void closeDriver(){
        if(!configuration.localBrowser().equalsIgnoreCase("firefox")){
            driver.close();
        }
        driver.quit();
        driver = null;
        logger.info("Disposing driver is finished.");
    }

}
