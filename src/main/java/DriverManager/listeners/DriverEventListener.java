package DriverManager.listeners;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;

public class DriverEventListener implements WebDriverEventListener {

    private Logger logger = LogManager.getLogger(DriverEventListener.class);;

    @Override
    public void beforeAlertAccept(WebDriver webDriver) {
        logger.info("Trying to accept alert");
    }

    @Override
    public void afterAlertAccept(WebDriver webDriver) {
        logger.info("Alert accepted");
    }

    @Override
    public void afterAlertDismiss(WebDriver webDriver) {
        logger.info("Alert dismissed");
    }

    @Override
    public void beforeAlertDismiss(WebDriver webDriver) {
        logger.info("Trying to dismiss alert");
    }

    @Override
    public void beforeNavigateTo(String s, WebDriver webDriver) {
        logger.info("Navigating to " + s);
    }

    @Override
    public void afterNavigateTo(String s, WebDriver webDriver) {
        logger.info("Navigated to " + s);
    }

    @Override
    public void beforeNavigateBack(WebDriver webDriver) {
        logger.info("Trying to navigate back");
    }

    @Override
    public void afterNavigateBack(WebDriver webDriver) {
        logger.info("Navigated back");
    }

    @Override
    public void beforeNavigateForward(WebDriver webDriver) {
        logger.info("Trying to navigate forward");
    }

    @Override
    public void afterNavigateForward(WebDriver webDriver) {
        logger.info("Navigated forward");
    }

    @Override
    public void beforeNavigateRefresh(WebDriver webDriver) {
        logger.info("Trying to refresh page");
    }

    @Override
    public void afterNavigateRefresh(WebDriver webDriver) {
        logger.info("Refreshed page");
    }

    @Override
    public void beforeFindBy(By by, WebElement webElement, WebDriver webDriver) {
        logger.info("Trying to find with locator " + by.toString());
    }

    @Override
    public void afterFindBy(By by, WebElement webElement, WebDriver webDriver) {
        logger.info("Founded element with locator " + by.toString());
    }

    @Override
    public void beforeClickOn(WebElement webElement, WebDriver webDriver) {
        logger.info("Trying to click on " + webElement.getTagName());
    }

    @Override
    public void afterClickOn(WebElement webElement, WebDriver webDriver) {
        logger.info("Clicked on " + webElement.getTagName());
    }

    @Override
    public void beforeChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {
        logger.info("Trying to change value of " + webElement.getTagName());
    }

    @Override
    public void afterChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {
        logger.info("Changed value of " + webElement.getTagName());
    }

    @Override
    public void beforeScript(String s, WebDriver webDriver) {
        logger.info("Trying to execute JS script: " + s);
    }

    @Override
    public void afterScript(String s, WebDriver webDriver) {
        logger.info("Executed JS script: " + s);
    }

    @Override
    public void beforeSwitchToWindow(String s, WebDriver webDriver) {
        logger.info("Trying to switch to " + s);
    }

    @Override
    public void afterSwitchToWindow(String s, WebDriver webDriver) {
        logger.info("Switched to " + s);
    }

    @Override
    public void onException(Throwable throwable, WebDriver webDriver) {
        logger.info("Exception occured " + throwable.getMessage());
    }

    @Override
    public <X> void beforeGetScreenshotAs(OutputType<X> outputType) {
        logger.info("Trying to get screenshot");
    }

    @Override
    public <X> void afterGetScreenshotAs(OutputType<X> outputType, X x) {
        logger.info("Done screenshot");
    }

    @Override
    public void beforeGetText(WebElement webElement, WebDriver webDriver) {
        logger.info("Trying to get text of element " + webElement.getTagName());
    }

    @Override
    public void afterGetText(WebElement webElement, WebDriver webDriver, String s) {
        logger.info("Taken text " + webElement.getTagName());
    }
}
