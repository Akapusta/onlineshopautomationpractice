package DriverManager;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

@Slf4j
public class DriverUtils {

    public static void setInitialConfiguration(){
//        DriverManager.getWebDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        DriverManager.getWebDriver().manage().window().maximize();
    }

    public static void navigateToPage(String pageUrl){
        log.info("Opening browser at: {}", pageUrl);
        DriverManager.getWebDriver().navigate().to(pageUrl);
        new WebDriverWait(DriverManager.getWebDriver(), 15).until(ExpectedConditions.urlToBe(pageUrl));
    }
}
