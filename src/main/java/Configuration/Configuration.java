package Configuration;


import org.aeonbits.owner.Config;

@Config.Sources("classpath:configuration.properties")
public interface Configuration extends Config {

    String applicationAddress();

    String gridHubUrl();

    String localBrowser();

    String driverType();
}
