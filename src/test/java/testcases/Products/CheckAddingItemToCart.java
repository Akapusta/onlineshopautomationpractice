package testcases.Products;

import PageObjects.BestsellersSectionMainPage;
import PageObjects.LayerCartPage;
import PageObjects.MainPage;
import PageObjects.ProductPage;
import io.qameta.allure.Description;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import testcases.BaseTest;

@Slf4j
public class CheckAddingItemToCart extends BaseTest {

    @Test
    @Description("The aim of this test is to check adding item to cart")
    public void checkAddingItemToCart(){
        var bp = new BestsellersSectionMainPage();
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(new MainPage().getBanner()));
        WebElement element = bp.getProductsImages().get(0);
        Actions actions = new Actions(driver);
        actions.moveToElement(element).perform();
        Assert.assertTrue(bp.getProductsImages().size()>0);
        bp.getProductsImages().get(0).click();
        var pp = new ProductPage();
        driver.switchTo().frame(1);
        pp.getSubmitButton().click();
        new WebDriverWait(driver, 20)
                .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".layer_cart_product h2")));
        var lp = new LayerCartPage();
        Assert.assertEquals(lp.getHeader().getText(), "Product successfully added to your shopping cart");
        lp.getCrossIcon().click();
    }
}
