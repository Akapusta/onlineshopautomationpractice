package testcases.LoginPage;

import PageObjects.LoginPage;
import io.qameta.allure.Description;
import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.Test;
import testcases.BaseTest;

@Slf4j
public class CheckContentOfLoginPage extends BaseTest {

    @Test
    @Description("The goal of this test is to check content of login page after navigating from main page")
    public void checkingContentOfLoginPage() {
        LoginPage lp = new LoginPage();
        lp.navigateToLoginPage();
        lp.checkingPageHeading();
    }
}
