package testcases.HomePage;

import PageObjects.MainPage;
import io.qameta.allure.Description;
import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.Test;
import testcases.BaseTest;

@Slf4j
public class CheckContentOfStartPage extends BaseTest {

    @Test
    @Description("The goal of this test is to check content of main page after opening application")
    public void checkingContentOfMainPage() {
        MainPage mp = new MainPage();
        mp.checkingBanner();
    }

}
