package testcases;

import Configuration.Configuration;

import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.aeonbits.owner.ConfigFactory;
import DriverManager.DriverManager;
import DriverManager.DriverUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

@Slf4j
public class BaseTest {

    private static Configuration configuration = ConfigFactory.create(Configuration.class);
    protected WebDriver driver;

    @BeforeClass
    @Step("Loading configuration from configuration.properties")
    public void setUp(){
        String applicationAddress = configuration.applicationAddress();
        DriverUtils.setInitialConfiguration();
        DriverManager.setWebDriver();
        DriverUtils.navigateToPage(applicationAddress);
        driver = DriverManager.getWebDriver();
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
        Assert.assertEquals(applicationAddress, driver.getCurrentUrl());
    }

    @AfterClass
    @Step("Closing browser")
    public void tearDown(){
        log.info("Closing browser");
        DriverManager.closeDriver();
    }

}
